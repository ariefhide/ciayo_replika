import React, { Component } from 'react';
import './App.css';
import Navigasi from './Navigasi';
import Header from './Header';
import Gallery from './Gallery';
import Footer from './Footer';
import AllComic from './AllComic';



class App extends Component {
  render() {
    return (
      <div className="App">
        <Navigasi/>
        <Header/>
        <AllComic/>
        <Gallery/>
        <Footer/>

        
          
      </div>
    );
  }
}

export default App;
