import React, { Component } from 'react';
import App from './App';
import logo from './logo.png';
import search from './search.png';
import login from './login.png';
import 'bulma/css/bulma.css';
import Header from './Header';
import allcomic from './allcomic.png';
import sch from './sch.png';
import challange from './challange.png';
import blog from './blog.png';
import AllComic from './AllComic';
import $ from 'jquery';
import { findDOMNode} from 'react-dom';

class Navigasi extends Component{
	constructor(props){
    super(props);


		this.state = {
			hero:"hero is-hidden",
			navbar:"navbar-menu navMenu ",
		};
	}
	handleHero = () => {
		if (this.state.hero === "hero is-hidden"){
			this.setState({hero: "hero "})
		}
		const el = findDOMNode(this.refs.toggle);
		$(el).slideToggle();
		
	}
	handleNavbar = () => {
		if (this.state.navbar === "navbar-menu navMenu "){
			this.setState({navbar: "navbar-menu navMenu is-active"})
		}else{
			this.setState({navbar:"navbar-menu navMenu "})
		}
	}


	render(){

		return(

		
			<div>
				<h1>
				
			  </h1>
			  <nav class="navbar is-fixed-top navHead" role="navigation" aria-label="main navigation">
			  	<div class="navbar-brand nav1">
				  	<a class="navbar-item" style={{marginRight:300}} href="#">
				  		<img src={logo} className="logo1" />
				  	</a>

				  	<a role="button" onClick={this.handleNavbar} id="nav-toggle" class="navbar-burger burger"  aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
				      <span aria-hidden="true"></span>
				      <span aria-hidden="true"></span>
				      <span aria-hidden="true"></span>
				    </a>
				    <a href="#" class="navbar-item">
				    	<i class="fa fa-search " onClick={this.handleHero}></i>
						</a>
				</div>
					

				<div id="navbarBasicExample " ref="toggle" class={this.state.navbar}>
    			  <div class="navbar-start " >

	      				<a href="#AllComic" class="navbar-item " style={{color:"#999999"}}>
	      				   	<img src={allcomic}/>
	      				  		ALL COMICS		

	        			</a>
	      				<a class="navbar-item " style={{color:"#999999"}}>
	        				<img src={sch} />
	        				SCHEDULE
	      				</a>
	      				<a href="#challange" class="navbar-item " style={{color:"#999999"}}>
	        				<img src={challange} />
	        				CHALLANGE
	      				</a>
	      				<a class="navbar-item" style={{color:"#999999"}}>
	        				<img src={blog} />
	        				BLOG
	      				</a>	  
	      		  </div>		 	
				
					<div class="navbar-end nav1 ">
				        <a class="navbar-item" href="#">
					  		LOGIN/REGISTER
					  		<img src={login} className="login" style={{ width:30, height:70}}/>
					  	</a>
					  				        
				    </div>
				 </div>   

			  </nav>

			  <section class={this.state.hero} ref="toggle">
				  <div class="hero-body">
				    <div class="container" style={{height:10, marginTop:30 }}>
				      <div class="field">
						 <div class="control">
						 	<div className="columns">
						 	  <div className="column is-12 is-offset-3">
						  		<input class="input" type="text" placeholder="Search Here" style={{width:550}}/>
						 	  </div>
						 	</div>	
						 </div>
					  </div>
				    </div>
				  </div>
				</section>

					
			</div>
		
		);
	}
}

export default Navigasi;