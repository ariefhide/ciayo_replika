import React, { Component } from 'react';
import 'bulma/css/bulma.css';
import editor1 from './editor1.jpg';
import new1 from './new1.jpg';
import commingsoon from './commingsoon.jpg';
import challange1 from './challange1.jpg';
import like from './like.png';

class Gallery extends Component{
  render(){
    return(
     		 <div>
				<section className="section" id="comic" style={{background:"white"}}>
					<div className="container" >
						<div className="columns">
							<div className="column is-3 is-offset-1">
								<h1 style={{fontSize:20}}>New Releases</h1>
							</div>
						</div>
						
						<div className="columns is-multiline " >

							<div className="column is-2 is-offset-1 is-half">
								<div className="card " >
									<div className="card-image newhead" >
										<figure className="image fullsized_image_holder " >
											<img src={new1} alt="gambar"  style={{objectFit:'cover'}} />
										</figure>
									</div>
											<h2 style={{width:"100%", color:"#7c6edf",marginLeft:20}}>spesial</h2>	
											<h1 style={{width:"100%", color:"black",marginLeft:20}} >LOVEPHOBIA</h1>								
										<div className="card-content" style={{ color:"black"}}>
											<a class="button is-link is-outlined">Subsribe</a>
										</div>
								</div>
							</div>
							<div className="column is-2 ">
								<div className="card " >
									<div className="card-image newhead" >
										<figure className="image fullsized_image_holder " >
											<img src={new1} alt="gambar"  style={{objectFit:'cover'}} />
										</figure>
									</div>
											<h2 style={{width:"100%", color:"#7c6edf",marginLeft:20}}>spesial</h2>	
											<h1 style={{width:"100%", color:"black",marginLeft:20}} >LOVEPHOBIA</h1>								
										<div className="card-content" style={{ color:"black"}}>
											<a class="button is-link is-outlined">Subsribe</a>
										</div>
								</div>
							</div>
							<div className="column is-2 ">
								<div className="card " >
									<div className="card-image newhead" >
										<figure className="image fullsized_image_holder " >
											<img src={new1} alt="gambar"  style={{objectFit:'cover'}} />
										</figure>
									</div>
											<h2 style={{width:"100%", color:"#7c6edf",marginLeft:20}}>spesial</h2>	
											<h1 style={{width:"100%", color:"black",marginLeft:20}} >LOVEPHOBIA</h1>								
										<div className="card-content" style={{ color:"black"}}>
											<a class="button is-link is-outlined">Subsribe</a>
										</div>
								</div>
							</div>
							<div className="column is-2 ">
								<div className="card " >
									<div className="card-image newhead" >
										<figure className="image fullsized_image_holder " >
											<img src={new1} alt="gambar"  style={{objectFit:'cover'}} />
										</figure>
									</div>
											<h2 style={{width:"100%", color:"#7c6edf",marginLeft:20}}>spesial</h2>	
											<h1 style={{width:"100%", color:"black",marginLeft:20}} >LOVEPHOBIA</h1>								
										<div className="card-content" style={{ color:"black"}}>
											<a class="button is-link is-outlined">Subsribe</a>
										</div>
								</div>
							</div>
							<div className="column is-2 ">
								<div className="card " >
									<div className="card-image newhead" >
										<figure className="image fullsized_image_holder " >
											<img src={new1} alt="gambar"  style={{objectFit:'cover'}} />
										</figure>
									</div>
											<h2 style={{width:"100%", color:"#7c6edf",marginLeft:20}}>spesial</h2>	
											<h1 style={{width:"100%", color:"black",marginLeft:20}} >LOVEPHOBIA</h1>								
										<div className="card-content" style={{ color:"black"}}>
											<a class="button is-link is-outlined">Subsribe</a>
										</div>
								</div>
							</div>
						</div>
					</div>
				</section>	

				<section className="section" style={{background:"#f0f0f0"}}>
					<div className="container" >
						<div className="columns">
							<div className="column is-3 is-offset-1">
								<h1 style={{fontSize:20}}>Coming Soon</h1>
							</div>
						</div>
						
						<div className="columns" >
							<div className="column is-4 ">
								<div className="card " >
									<div className="card-image newhead" >
										<figure className="image fullsized_image_holder " >
											<img src={commingsoon} alt="gambar"  style={{objectFit:'cover', height:200}} />
										</figure>
									</div>
								</div>
							<footer class="card-footer" style={{background:"white"}}>
							    
							    <a class="card-footer-item" >
							      <span>
							      	
							        <p >Horror</p>
							        <h3>DreadOut : The Untolds</h3><br/>
							        <p>Usaha Linda untuk menyelamatkan teman-temanya dari kota</p>
							      </span>
							    
							    </a>

							    
							    <a class="card-footer-item" style={{background:"#ed3936", color:"white"}}>
							      <span>
							      	
							       <p>Release Date</p>
							       <h1>01 NOV</h1>
							       <p>20:00</p>
							      	
							      </span>
							    
							    </a>
							  </footer>
							</div>

							<div className="column is-4 ">
								<div className="card " >
									<div className="card-image newhead" >
										<figure className="image fullsized_image_holder " >
											<img src={commingsoon} alt="gambar"  style={{objectFit:'cover', height:200}} />
										</figure>
									</div>
								</div>
							<footer class="card-footer" style={{background:"white"}}>
							    
							    <a class="card-footer-item" >
							      <span>
							      	
							        <p >Horror</p>
							        <h3>DreadOut : The Untolds</h3><br/>
							        <p>Usaha Linda untuk menyelamatkan teman-temanya dari kota</p>
							      </span>
							    
							    </a>
							    <a class="card-footer-item" style={{background:"#ed3936", color:"white"}}>
							      <span>
							      	
							       <p>Release Date</p>
							       <h1>01 NOV</h1>
							       <p>20:00</p>
							      	
							      </span>
							    
							    </a>
							  </footer>
							</div>	  
							<div className="column is-4 ">
								<div className="card " >
									<div className="card-image newhead" >
										<figure className="image fullsized_image_holder " >
											<img src={commingsoon} alt="gambar"  style={{objectFit:'cover', height:200}} />
										</figure>
									</div>
								</div>
							<footer class="card-footer" style={{background:"white"}}>
							    
							    <a class="card-footer-item" >
							      <span>
							      	
							        <p >Horror</p>
							        <h3>DreadOut : The Untolds</h3><br/>
							        <p>Usaha Linda untuk menyelamatkan teman-temanya dari kota</p>
							      </span>
							    
							    </a>

							    
							    <a class="card-footer-item" style={{background:"#ed3936", color:"white"}}>
							      <span>
							      	
							       <p>Release Date</p>
							       <h1>01 NOV</h1>
							       <p>20:00</p>
							      	
							      </span>
							    
							    </a>
							  </footer>
							</div>	  	  
						</div>	
					</div>	
				</section>
				
				<section id="challange" className="section" style={{background:"white"}}>
					<div className="container" >
						<div className="columns">
							<div className="column is-3 is-offset-1">
								<h1 style={{fontSize:20}}>CIAYO Comics Challenge</h1>
							</div>
						</div>
						
						<div className="columns is-multiline " >
							<div className="column is-2 is-offset-1 ">
								<div className="card " >
									<div className="card-image newhead" style={{border:"none"}}>
										<figure className="image fullsized_image_holder " >
											<img src={challange1} alt="gambar"  style={{objectFit:'cover'}} />
										</figure>
									</div>
											<h1 style={{width:"100%", color:"#7c6edf", marginLeft:5}}>Far Away</h1>	
											<h2 style={{width:"100%", color:"black", marginLeft:5}} >NECO-TEA</h2>								
										<div className="card-content" style={{ color:"black"}}>
											<p>
											<img src={like}/>
											180 Likes
											</p>
										</div>
								</div>
							</div>
							<div className="column is-2 ">
								<div className="card " >
									<div className="card-image newhead" style={{border:"none"}}>
										<figure className="image fullsized_image_holder " >
											<img src={challange1} alt="gambar"  style={{objectFit:'cover'}} />
										</figure>
									</div>
											<h1 style={{width:"100%", color:"#7c6edf", marginLeft:5}}>Far Away</h1>	
											<h2 style={{width:"100%", color:"black", marginLeft:5}} >NECO-TEA</h2>								
										<div className="card-content" style={{ color:"black"}}>
											<p>
											<img src={like}/>
											180 Likes
											</p>
										</div>
								</div>
							</div>

							<div className="column is-2 ">
								<div className="card " >
									<div className="card-image newhead" style={{border:"none"}}>
										<figure className="image fullsized_image_holder " >
											<img src={challange1} alt="gambar"  style={{objectFit:'cover'}} />
										</figure>
									</div>
											<h1 style={{width:"100%", color:"#7c6edf", marginLeft:5}}>Far Away</h1>	
											<h2 style={{width:"100%", color:"black", marginLeft:5}} >NECO-TEA</h2>								
										<div className="card-content" style={{ color:"black"}}>
											<p>
											<img src={like}/>
											180 Likes
											</p>
										</div>
								</div>
							</div>

							<div className="column is-2 ">
								<div className="card " >
									<div className="card-image newhead" style={{border:"none"}}>
										<figure className="image fullsized_image_holder " >
											<img src={challange1} alt="gambar"  style={{objectFit:'cover'}} />
										</figure>
									</div>
											<h1 style={{width:"100%", color:"#7c6edf", marginLeft:5}}>Far Away</h1>	
											<h2 style={{width:"100%", color:"black", marginLeft:5}} >NECO-TEA</h2>								
										<div className="card-content" style={{ color:"black"}}>
											<p>
											<img src={like}/>
											180 Likes
											</p>
										</div>
								</div>
							</div>
							<div className="column is-2 ">
								<div className="card " >
									<div className="card-image newhead" style={{border:"none"}}>
										<figure className="image fullsized_image_holder " >
											<img src={challange1} alt="gambar"  style={{objectFit:'cover'}} />
										</figure>
									</div>
											<h1 style={{width:"100%", color:"#7c6edf", marginLeft:5}}>Far Away</h1>	
											<h2 style={{width:"100%", color:"black", marginLeft:5}} >NECO-TEA</h2>								
										<div className="card-content" style={{ color:"black"}}>
											<p>
											<img src={like}/>
											180 Likes
											</p>
										</div>
								</div>
							</div>

						</div>
					</div>
				</section>												
      		</div>
    );
  }
}

export default Gallery;