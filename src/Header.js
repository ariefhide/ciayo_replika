import React, { Component } from 'react';
import 'bulma/css/bulma.css';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import jumbotron1 from './jumbotron1.jpg';
import jumbotron2 from './jumbotron2.jpg';
import jumbotron3 from './jumbotron3.jpg';
import share from './share.jpg';
import ReactDOM from 'react-dom';
import appstore from './appstore.png';
import playstore from './playstore.png';


class Header extends Component{
  render(){
    return(
      <div>
		  <Carousel showThumbs={false} autoPlay interval={3000} infiniteLoop class="carousel" style={{hight:300}}>
                <div>
                    <img src={jumbotron1} />
                   
                </div>
                <div>
                    <img src={jumbotron2} />
                    
                </div>
                <div>
                    <img src={jumbotron3} />
                    
                </div>
            </Carousel>
            <section class="hero con" style={{backgroundImage: `url(${share})`, backgroundRepeat: "no-repeat",backgroundPosition:"right",}}>

			  
			    
			    	<div class="columns is-gapless is-multiline" style={{marginTop:0, color:"white"}}>
			    		<div class="column is-5 is-offset-1" >
			    			<h1 style={{fontSize:"2.2vw", marginLeft:100}}>Bring your comic in your pocket.</h1>
			    		</div>
			    		<div class="columns is-multiline is-mobile " >
			    			<a class="column is-5" >
			    			  <img src={appstore} style={{borderRadius:5}}/>
			    			</a>
			    			<a class="column is-5">
			    			  <img src={playstore} style={{borderRadius:5}}/>
			    			</a>
			    		</div>
			    	</div>
			</section>
			<br/>
			<br/>
      </div>
    );
  }
}



export default Header;
