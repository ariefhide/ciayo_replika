import React, { Component } from 'react';
import 'bulma/css/bulma.css';
import editor1 from './editor1.jpg';


class AllComic extends Component{
  render(){
    return(
    		<div>
				<section className="section" id="AllComic" style={{background:"#f0f0f0"}}>
					<div className="container" >
						<div className="columns">
							<div className="column is-3 is-offset-1">
								<h1 style={{fontSize:20}}>Editor's Choices</h1>
							</div>
						</div>
						
						<div className="columns is-tablet" >

							<div className="column is-3 is-offset-1">
								<div className="card"  >
									<div className="card-image newhead" >
										<figure className="image is-2by1"  >
											<img src={editor1} alt="gambar"  style={{objectFit:'cover' }} />
										</figure>
									</div>
									<div style={{background:"#9d92e7", width:"100%", color:"white"}}>
										<h2 style={{marginLeft:20}}>spesial</h2>
									</div>
										<div className="card-content" style={{background:"#7c6edf", width:"100%", color:"white"}}>
											<div >
												<div >
													<h1 style={{fontSize:"2vw"}}>LOVEPHOBIA</h1>
												</div>
												<div >
													<h3 style={{fontSize:"1vw"}}>Orang bilang cinta itu berwarna merah muda</h3>
												</div>
											</div>
											<div class="overlay">
											    <div class="text">READ NOW</div>
											  </div>
										</div>
								</div>

							</div>
							<div className="column is-3 ">
								<div className="card container1" >
									<div className="card-image newhead" >
										<figure className="image is-2by1"  >
											<img src={editor1} alt="gambar"  style={{objectFit:'cover'}} />
										</figure>
									</div>
									<div style={{background:"#9d92e7", width:"100%", color:"white"}}>
										<h2 style={{marginLeft:20}}>spesial</h2>
									</div>
										<div className="card-content" style={{background:"#7c6edf", width:"100%", color:"white"}}>
											<div >
												<div >
													<h1 style={{fontSize:"2vw"}}>LOVEPHOBIA</h1>
												</div>
												<div >
													<h3 style={{fontSize:"1vw"}}>Orang bilang cinta itu berwarna merah muda</h3>
												</div>
											</div>
											<div class="overlay">
											    <div class="text">READ NOW</div>
											  </div>
										</div>
								</div>

							</div>
							<div className="column is-3 ">
								<div className="card container1" >
									<div className="card-image newhead" >
										<figure className="image is-2by1"  >
											<img src={editor1} alt="gambar"  style={{objectFit:'cover'}} />
										</figure>
									</div>
									<div style={{background:"#9d92e7", width:"100%", color:"white"}}>
										<h2 style={{marginLeft:20}}>spesial</h2>
									</div>
										<div className="card-content" style={{background:"#7c6edf", width:"100%", color:"white"}}>
											<div >
												<div >
													<h1 style={{fontSize:"2vw"}}>LOVEPHOBIA</h1>
												</div>
												<div >
													<h3 style={{fontSize:"1vw"}}>Orang bilang cinta itu berwarna merah muda</h3>
												</div>
											</div>
											<div class="overlay">
											    <div class="text">READ NOW</div>
											  </div>
										</div>
								</div>

							</div>
						</div>
					</div>
				</section>
			</div>
		);
	}
}

export default AllComic;				