import React, { Component } from 'react';
import logo2 from './logo2.png';
import 'bulma/css/bulma.css';


class Footer extends  Component{
  render(){
    return(
      <div>
      			<section className="section" style={{background:"#ae2b26"}}>
					<div className="container" >
						<div className="columns">
							<div className="column is-9 is-offset-1">
								<h1 style={{color:"white",fontSize:"25px"}}>CIAYO Comics - Tempat Baca Komik Online Terlengkap Untuk Milenial Indonesia</h1>
									<h2 style={{color:"white", fontSize:"20px"}}>Kegiatan baca komik online dan webtoon gratis jadi serba mudah di CIAYO Comics</h2><br/>
								<h3 style={{color:"white", fontSize:"15px"}}>CIAYO Comics adalah platform baca komik online terlengkap bagi milenial Indonesia. Di sini, kamu bisa baca komik online Indonesia yang beraneka ragam dari author favoritmu. Tersedia komik gaya Jepang, web komik, webtoon gratis, atau komik lokal. Dari segi genre, CIAYO Comics juga punya komik romance, comedy, horror, slice of life, drama, action, fantasy, dan juga genre special yang pastinya bisa memenuhi seleramu. Segala webtoon karya anak Indonesia di sini dapat kamu baca secara gratis. Ingin membaca belakangan? Kamu juga bisa download komik dan langsung membacanya nanti. Tidak hanya itu, kamu juga mendapatkan update webtoon gratis terbaru langsung dari author kesukaanmu. Dan, kamu bahkan juga bisa meninggalkan komentar dan berinteraksi dengan mereka. Yuk, tunggu apa lagi? Pantengin CIAYO Comics biar kamu bisa terhubung dengan komunitas komik terbesar untuk milenial Indonesia!</h3>
							</div>	
						</div>
					</div>
				</section>

				<section className="section " style={{background:"#f7f7f7", textAlign: "center"}}>
					<div className="container" style={{background:"#f7f7f7"}}>
						<div className="columns " >
							<div className="column is-2">
								<img src={logo2} />
							</div>
							<div className="column is-1 is-offset-1">
								<a className="af">About Us</a>
							</div>
							<div className="column is-2">
								<a className="af">Terms & Conditions</a>
							</div>
							<div className="column is-1">
								<a className="af">Privacy Policy</a>
							</div>
							<div className="column is-1">
								<a className="af">F.A.Q</a>
							</div>
							<div className="column is-1">
								<a className="af">Contact Us</a>
							</div>
							<div className="column is-2 is-offset-1">
								<p className="af">Comic Language</p>
								<div className="columns">
									<div className="column is-5">
										<a style={{color:"white",background:"#bdbdbd"}} class="button ">English</a>
									</div>
									<div className="column is-5 ">
										<a class="button " style={{background:"#df6e22",color:"white"}}>Indonesia</a>
									</div>
								</div>
							</div>	
						</div>
					</div>
				</section>

				
      </div>
    );
  }
}
export default Footer;